import { YellowBox, AppRegistry } from 'react-native';
import App from './src/App';

YellowBox.ignoreWarnings([
  'Warning: isMounted(...) is deprecated in plain JavaScript React classes',
  'Remote debugger is in a background tab which may cause apps to perform slowly',
  'Warning: Failed prop type',
  'Warning: Can\'t call setState (or forceUpdate) on an unmounted component',
]);

AppRegistry.registerComponent('moviesProjectRN', () => App);
