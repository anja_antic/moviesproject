// @flow
import React, { Component } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import { bindActionCreators } from 'redux';
import type { Dispatch as ReduxDispatch } from 'redux';
import { connect } from 'react-redux';
import YouTube from 'react-native-youtube';

import type { Card } from '../../types/Card';
import { addToFavourite } from '../../actions/FavouriteMovieActions';
import { getVideo, setVideoUrlEmpty } from '../../actions/VideoPreviewActions';
import styles from './styles';
import { backButtonInDetails } from '../../constants/styleConstants';
import AddButton from '../../components/addButton/AddButton';
import { CloseOnWhite } from '../../constants/imageConstants';
import { getPosterUrl, formatDate } from '../../utils/helpers';

type Props = {
  setActiveMovie: (movieItem: Card) => Object,
  navigation: Object,
  addToFavourite: (movieItem: Card) => Object,
  videoUrl: string,
  getVideo: (cardId: number) => Object,
  setVideoUrlEmpty: () => Object,
};

class MovieDetailsContainer extends Component<Props> {
  componentDidMount() {
    this.props.getVideo(this.props.navigation.state.params.card.id);
  }

  componentWillUnmount() {
    this.props.setVideoUrlEmpty();
  }

  render() {
    const { navigation, addToFavourite, videoUrl } = this.props;
    const { card } = this.props.navigation.state.params;
    console.log('aktivan', this.props.navigation.state.params.card);
    console.log('trenutni url', videoUrl);
    console.log('image', card.poster_path);
    return (
      <ScrollView style={styles.scrollView}>
        <View style={styles.mainView}>
          <Image
            resizeMode="cover"
            style={styles.poster}
            source={{
              uri: getPosterUrl(card.poster_path),
            }}
          />
          <View style={styles.addButton}>
            <AddButton addToFavourite={() => addToFavourite(card)} />
          </View>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={backButtonInDetails.backButtonTouch}
          >
            <Image style={backButtonInDetails.backButtonIcon} source={CloseOnWhite} />
          </TouchableOpacity>
        </View>
        <View style={styles.textView}>
          <Text style={styles.title}>
            {card.title}
          </Text>
          <Text style={styles.overview}>
            Overview
          </Text>
          <Text style={styles.description}>
            {card.overview}
          </Text>
          <Text style={styles.date}>
            Release date: {formatDate(card.release_date)}
          </Text>
          <View style={styles.videoView}>
          {videoUrl !== '' ?
            <YouTube
              apiKey="AIzaSyCtfnAL3-6w4ydpluRhp-Q8HMIGk9rf8k8" // dobijen preko google account-a
              videoId={videoUrl}
              style={styles.youtubePlayer}
            /> : null
          }
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  videoUrl: state.videoUrl.videoUrl,
});
const mapDispatchToProps = (dispatch: ReduxDispatch): Function => (
  bindActionCreators({
    addToFavourite,
    getVideo,
    setVideoUrlEmpty,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(MovieDetailsContainer);
