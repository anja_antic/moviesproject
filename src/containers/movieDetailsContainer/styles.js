import { Dimensions } from 'react-native';
import { Fonts } from '../../utils/Fonts';
import { headerBackgroundColor } from '../../constants/styleConstants';

const styles = {
  scrollView: {
    backgroundColor: headerBackgroundColor,
  },
  mainView: {
    flex: 1,
  },
  textView: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 15,
  },
  poster: {
    width: '100%',
    height: Dimensions.get('window').height / 1.5,
  },
  title: {
    fontFamily: Fonts.MontserratBlack,
    fontSize: 20,
    color: 'white',
    opacity: 0.7,
    marginBottom: 15,
  },
  overview: {
    fontFamily: Fonts.MontserratRegular,
    fontSize: 15,
    color: 'white',
    opacity: 0.7,
    marginBottom: 5,
  },
  description: {
    fontFamily: Fonts.MontserratLight,
    fontSize: 13,
    color: 'white',
    opacity: 0.7,
  },
  date: {
    fontFamily: Fonts.MontserratRegular,
    fontSize: 15,
    color: 'white',
    opacity: 0.7,
    marginTop: 10,
  },
  addButton: {
    position: 'absolute',
    bottom: 15,
    left: 15,
  },
  videoView: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  youtubePlayer: {
    height: Dimensions.get('window').width / 1.5,
    width: Dimensions.get('window').width - 30,
    marginTop: 10,
  },
};

export default styles;
