// @flow
import React, { Component } from 'react';
import type { Dispatch as ReduxDispatch } from 'redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { View } from 'react-native';

import type { Card } from '../../types/Card';
import { listsPaddingBottom } from '../../constants/styleConstants';
import fetchTopRatedMovies from '../../actions/TopRatedMovieActions';
import { addToFavourite, removeFromFavourite } from '../../actions/FavouriteMovieActions';
import RefreshableFlatList from '../../components/refreshableFlatList/RefreshableFlatList';
import Header from '../../components/headers/Header';
import getMoviesByReleaseDate from '../../actions/ReleaseDateActions';

type Props = {
  fetchTopRatedMovies: () => Promise<void>,
  topRatedMovies: Array<Object>,
  navigation: Object,
  addToFavourite: (movieItem: Card) => Object,
  removeFromFavourite: (movieItem: Card) => Object,
  getMoviesByReleaseDate: (date: string) => Object,
}

class ListOfTopRatedContainer extends Component<Props> {
  render() {
    const {
      navigation,
      fetchTopRatedMovies,
      removeFromFavourite,
      topRatedMovies,
      addToFavourite,
      getMoviesByReleaseDate,
    } = this.props;
    return (
      <View style={{ paddingBottom: listsPaddingBottom }}>
        <Header title="TOP RATED" navigation={navigation} />
        <RefreshableFlatList
          getMovies={fetchTopRatedMovies}
          movies={topRatedMovies}
          addToFavourite={(item) => addToFavourite(item)}
          removeFromFavourite={(item) => removeFromFavourite(item)}
          navigation={navigation}
          getMoviesByReleaseDate={(date) => getMoviesByReleaseDate(date)}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  topRatedMovies: state.topRated.topRatedMovies,
});
const mapDispatchToProps = (dispatch: ReduxDispatch): Function => (
  bindActionCreators({
    fetchTopRatedMovies,
    addToFavourite,
    removeFromFavourite,
    getMoviesByReleaseDate,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(ListOfTopRatedContainer);
