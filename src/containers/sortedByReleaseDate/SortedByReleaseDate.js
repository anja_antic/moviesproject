// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FlatList, View } from 'react-native';
import { bindActionCreators } from 'redux';
import type { Dispatch as ReduxDispatch } from 'redux';

import Statusbar from '../../components/statusBar/Statusbar';
import HeaderBackButton from '../../components/headers/HeaderBackButton';
import { keyExtractor } from '../../utils/helpers';
import { addToFavourite } from '../../actions/FavouriteMovieActions';
import MovieCard from '../../components/movieCard/MovieCard';
import type { Card } from '../../types/Card';

type Props = {
  chosenDate: string,
  navigation: Object,
  moviesByDate: Array<Card>,
  addToFavourite: (movieItem: Card) => Object,
};

class SortedByReleaseDate extends Component<Props> {
  render() {
    const { chosenDate, navigation, moviesByDate, addToFavourite } = this.props;
    return (
      <View>
        <Statusbar />
        <HeaderBackButton title={`RELEASE DATE:  ${chosenDate}`} navigation={navigation} />
        <FlatList
          numColumns={2}
          data={moviesByDate}
          keyExtractor={(item, index) => keyExtractor(item, index)}
          renderItem={({ item }) => (
            <MovieCard
              navigation={navigation}
              card={item}
              addToFavourite={() => addToFavourite(item)}
            />
          )}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  moviesByDate: state.popularMovies.moviesByDate,
  chosenDate: state.popularMovies.chosenDate,
});

const mapDispatchToProps = (dispatch: ReduxDispatch): Function => (
  bindActionCreators({ addToFavourite }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(SortedByReleaseDate);
