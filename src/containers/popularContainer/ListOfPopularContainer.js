// @flow
import React, { Component } from 'react';
import { View } from 'react-native';
import { bindActionCreators } from 'redux';
import type { Dispatch as ReduxDispatch } from 'redux';
import { connect } from 'react-redux';

import { listsPaddingBottom } from '../../constants/styleConstants';
import { fetchPopularMovies } from '../../actions/PopularMovieActions';
import { addToFavourite, removeFromFavourite } from '../../actions/FavouriteMovieActions';
import getMoviesByReleaseDate from '../../actions/ReleaseDateActions';
import RefreshableFlatList from '../../components/refreshableFlatList/RefreshableFlatList';
import Header from '../../components/headers/Header';
import type { Card } from '../../types/Card';

type Props = {
  fetchPopularMovies: () => Promise<void>,
  popularMovies: Array<Card>,
  navigation: Object,
  addToFavourite: (movieItem: Card) => Object,
  removeFromFavourite: (movieItem: Card) => Object,
  getMoviesByReleaseDate: (date: string) => Object,
}

class ListOfPopularContainer extends Component<Props> {
  render() {
    const {
      navigation,
      fetchPopularMovies,
      removeFromFavourite,
      popularMovies,
      addToFavourite,
      getMoviesByReleaseDate,
    } = this.props;
    return (
      <View style={{ paddingBottom: listsPaddingBottom }}>
        <Header title="POPULAR" navigation={navigation} />
        <RefreshableFlatList
          getMovies={fetchPopularMovies}
          movies={popularMovies}
          addToFavourite={item => addToFavourite(item)}
          removeFromFavourite={item => removeFromFavourite(item)}
          navigation={navigation}
          getMoviesByReleaseDate={date => getMoviesByReleaseDate(date)}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  popularMovies: state.popularMovies.popularMovies,
});
const mapDispatchToProps = (dispatch: ReduxDispatch): Function => (
  bindActionCreators({
    fetchPopularMovies,
    addToFavourite,
    removeFromFavourite,
    getMoviesByReleaseDate,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(ListOfPopularContainer);
