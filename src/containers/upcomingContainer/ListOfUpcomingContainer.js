// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import type { Dispatch as ReduxDispatch } from 'redux';
import { connect } from 'react-redux';
import { View } from 'react-native';

import { listsPaddingBottom } from '../../constants/styleConstants';
import type { Card } from '../../types/Card';
import fetchUpcomingMovies from '../../actions/UpcomingMovieActions';
import { addToFavourite, removeFromFavourite } from '../../actions/FavouriteMovieActions';
import getMoviesByReleaseDate from '../../actions/ReleaseDateActions';
import RefreshableFlatList from '../../components/refreshableFlatList/RefreshableFlatList';
import Header from '../../components/headers/Header';

type Props = {
  fetchUpcomingMovies: () => Promise<void>,
  upcomingMovies: Array<Object>,
  navigation: Object,
  addToFavourite: (movieItem: Card) => Object,
  removeFromFavourite: (movieItem: Card) => Object,
  getMoviesByReleaseDate: (date: string) => Object,
}

class ListOfUpcomingContainer extends Component<Props> {
  render() {
    const {
      navigation,
      fetchUpcomingMovies,
      removeFromFavourite,
      upcomingMovies,
      addToFavourite,
      getMoviesByReleaseDate,
    } = this.props;
    return (
      <View style={{ paddingBottom: listsPaddingBottom }}>
        <Header title="UPCOMING" navigation={navigation} />
        <RefreshableFlatList
          getMovies={fetchUpcomingMovies}
          movies={upcomingMovies}
          addToFavourite={item => addToFavourite(item)}
          removeFromFavourite={item => removeFromFavourite(item)}
          navigation={navigation}
          getMoviesByReleaseDate={date => getMoviesByReleaseDate(date)}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  upcomingMovies: state.upcoming.upcomingMovies,
});
const mapDispatchToProps = (dispatch: ReduxDispatch): Function => (
  bindActionCreators({
    fetchUpcomingMovies,
    addToFavourite,
    removeFromFavourite,
    getMoviesByReleaseDate,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(ListOfUpcomingContainer);
