// @flow
import React, { Component } from 'react';
import { FlatList, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import type { Dispatch as ReduxDispatch } from 'redux';

import { getActors } from '../../actions/ActorsActions';
import { keyExtractor } from '../../utils/helpers';
import styles from './styles';
import ActorCard from '../../components/actorCard/ActorCard';
import Header from '../../components/headers/Header';
import SearchActors from '../../components/searchActors/SearchActors';
import type { Actor } from '../../types/Actor';

type Props = {
  getActors: () => void,
  actors: Array<Actor>,
  navigation: Object,
}

type State = {
  searching: boolean,
}

class ActorsContainer extends Component<Props, State> {
  state = {
    page: 1,
    searching: false,
  };

  componentDidMount() {
    const { getActors } = this.props;
    getActors(this.state.page);
  }

  pagination() {
    this.props.getActors(this.state.page + 1, true);
    this.setState({ page: this.state.page + 1 });
  }

  isSearching(bool: boolean) {
    this.setState({ searching: bool });
  }

  renderList() {
    const { searching } = this.state;
    const { actors, navigation } = this.props;
    if (searching === false) {
      return (
        <View style={styles.viewStyle}>
          <FlatList
            numColumns={2}
            onEndReached={() => this.pagination()}
            onEndReachedThreshold={0.1}
            data={actors}
            keyExtractor={(item, index) => keyExtractor(item, index)}
            renderItem={({ item }) => (
              <ActorCard
                actor={item}
                poster={item.profile_path}
                navigation={navigation}
              />
            )}
          />
        </View>
      );
    }
    return null;
  }

  render() {
    const { navigation, actors } = this.props;
    console.log('searching', this.state.searching);
    console.log('allPageActors', this.props.actors);
    return (
      <View>
        <Header title="ACTORS" navigation={navigation} />
        <SearchActors
          actors={actors}
          navigation={navigation}
          isSearching={(bool) => this.isSearching(bool)}
        />
        {this.renderList()}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  actors: state.actors.actors,
});
const mapDispatchToProps = (dispatch: ReduxDispatch): Function => (
  bindActionCreators({ getActors }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(ActorsContainer);