// @flow
import React, { Component } from 'react';
import { FlatList, View } from 'react-native';
import { bindActionCreators } from 'redux';
import type { Dispatch as ReduxDispatch } from 'redux';
import { connect } from 'react-redux';

import type { Card } from '../../types/Card';
import { keyExtractor } from '../../utils/helpers';
import { removeFromFavourite } from '../../actions/FavouriteMovieActions';
import { favouriteMoviesPaddingBottom } from '../../constants/styleConstants';
import FavouriteMovieCard from '../../components/favouriteMovieCard/FavouriteMovieCard';
import Header from '../../components/headers/Header';

type Props = {
  favouriteMovies: Array<Card>,
  navigation: Object,
  removeFromFavourite: (movieItem: Card) => Object,
}

class FavouriteMoviesContainer extends Component<Props> {
  render() {
    const { favouriteMovies, navigation, removeFromFavourite } = this.props;
    return (
      <View style={{ paddingBottom: favouriteMoviesPaddingBottom }}>
        <Header title="FAVOURITES" navigation={this.props.navigation} />
      <FlatList
        data={favouriteMovies}
        keyExtractor={(item, index) => keyExtractor(item, index)}
        renderItem={({ item }) => (
          <FavouriteMovieCard
            removeFromFavourite={() => removeFromFavourite(item)}
            navigation={navigation}
            card={item}
          />
        )}
      />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  favouriteMovies: state.favouriteMovies.favouriteMovies,
});
const mapDispatchToProps = (dispatch: ReduxDispatch): Function => (
  bindActionCreators({ removeFromFavourite }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(FavouriteMoviesContainer);
