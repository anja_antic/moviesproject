// @flow
import React, { Component } from 'react';
import { View, FlatList, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import type { Dispatch as ReduxDispatch } from 'redux';
import { connect } from 'react-redux';

import { sortedByGenrePaddingBottom } from '../../constants/styleConstants';
import { addToFavourite } from '../../actions/FavouriteMovieActions';
import { keyExtractor } from '../../utils/helpers';
import HeaderBackButton from '../../components/headers/HeaderBackButton';
import MovieCard from '../../components/movieCard/MovieCard';
import SearchBox from '../../components/searchBox/SearchBox';
import type { Card } from '../../types/Card';
import styles from './styles';

type Props = {
  genre: Array<Card>,
  navigation: Object,
  addToFavourite: (item: Card) => Object,
  genreName: string,
};

type State = {
  searching: boolean,
};

class SortedByGenreContainer extends Component<Props, State> {
  state = {
    searching: false,
  };

  isSearching(bool) {
    this.setState({ searching: bool });
  }

  renderGenre() {
    const { genre, navigation, addToFavourite } = this.props;
    const { searching } = this.state;
    if (searching === false) {
      return (
        <FlatList
          numColumns={2}
          data={genre}
          keyExtractor={(item, index) => keyExtractor(item, index)}
          renderItem={({ item }) => (
            <MovieCard
              navigation={navigation}
              card={item}
              addToFavourite={() => addToFavourite(item)}
            />
          )}
        />
      );
    }
    return (
      <Text style={styles.message}>
        Sorry, there are no movies under that name
      </Text>
    );
  }

  render() {
    const { genreName, navigation, genre } = this.props;
    return (
      <View style={{ paddingBottom: sortedByGenrePaddingBottom }}>
        <HeaderBackButton title={genreName} navigation={navigation} />
        <SearchBox
          isSearching={bool => this.isSearching(bool)}
          movies={genre}
        />
        {this.renderGenre()}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  genre: state.popularMovies.genre,
  genreName: state.popularMovies.genreName,
});
const mapDispatchToProps = (dispatch: ReduxDispatch): Function => (
  bindActionCreators({ addToFavourite }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(SortedByGenreContainer);
