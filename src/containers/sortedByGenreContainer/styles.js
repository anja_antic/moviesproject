import { Fonts } from '../../utils/Fonts';

const styles = {
  message: {
    fontFamily: Fonts.MontserratRegular,
    fontSize: 15,
    margin: 20,
  },
};

export default styles;
