// @flow
import React, { Component } from 'react';
import { View, ScrollView, Text, Image, TouchableOpacity } from 'react-native';
import { bindActionCreators } from 'redux';
import type { Dispatch as ReduxDispatch } from 'redux';
import { connect } from 'react-redux';
import { Transition } from 'react-navigation-fluid-transitions';

import { getActorDetails, clearActorDetails } from '../../actions/ActorsActions';
import { getProfileUrl, formatDate } from '../../utils/helpers';
import { backButtonInDetails } from '../../constants/styleConstants';
import { CloseOnWhite } from '../../constants/imageConstants';
import type { ActorDetails } from '../../types/ActorDetails';
import styles from './styles';

type Props = {
  getActorDetails: (actorId: number) => Object,
  navigation: Object,
  clearActorDetails: () => Object,
  activeActor: ActorDetails,
}

class ActorDetailsContainer extends Component<Props> {
  componentDidMount() {
    this.props.getActorDetails(this.props.navigation.state.params.actorId);
  }

  componentWillUnmount() {
    this.props.clearActorDetails();
  }

  render() {
    const { activeActor, navigation } = this.props;
    const { profilePath, actorId } = navigation.state.params;
    console.log('ID', actorId);
    return (
      <ScrollView
        style={styles.scrollView}
      >
        <View style={styles.mainView}>
          <Transition shared={`image${actorId}`}>
            <Image
              resizeMode="cover"
              source={{ uri: getProfileUrl(profilePath) }}
              style={styles.posterStyle}
            />
          </Transition>
          <View style={styles.textView}>
            <Text style={styles.name}>{activeActor.name}</Text>
            <Text style={styles.actorDetails}>Date of birth: {formatDate(activeActor.birthday)}
              {'\n'}{'\n'}
              Deathday: {formatDate(activeActor.deathday)}
              {'\n'}{'\n'}
              Place of birth: {activeActor.place_of_birth}
              {'\n'}{'\n'}
              Biography: {activeActor.biography}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={backButtonInDetails.backButtonTouch}
          >
            <Image style={backButtonInDetails.backButtonIcon} source={CloseOnWhite} />
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  activeActor: state.actors.activeActor,
});

const mapDispatchToProps = (dispatch: ReduxDispatch): Function => (
  bindActionCreators({
    getActorDetails,
    clearActorDetails,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(ActorDetailsContainer);
