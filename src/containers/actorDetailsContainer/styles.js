import { Dimensions } from 'react-native';

import { headerBackgroundColor } from '../../constants/styleConstants';
import { Fonts } from '../../utils/Fonts';

const styles = {
  scrollView: {
    backgroundColor: headerBackgroundColor,
  },
  mainView: {
    flex: 1,
  },
  posterStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').width,
  },
  textView: {
    padding: 15,
  },
  name: {
    fontFamily: Fonts.MontserratBlack,
    fontSize: 20,
    color: 'white',
    opacity: 0.7,
    marginBottom: 15,
  },
  actorDetails: {
    fontFamily: Fonts.MontserratRegular,
    fontSize: 13,
    color: 'white',
    opacity: 0.7,
    marginBottom: 5,
  },
};

export default styles;
