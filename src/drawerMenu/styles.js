import { headerTitleColor, headerFontWeight, iconsColor } from '../constants/styleConstants';

const styles = {
  mainView: {
    paddingBottom: 70,
    paddingTop: 40,
  },
  lineSeparator: {
    backgroundColor: iconsColor,
    padding: 1,
    marginBottom: 10,
  },
  title: {
    color: iconsColor,
    fontSize: 19,
    fontWeight: headerFontWeight,
    marginLeft: 40,
    marginBottom: 5,
  },
  drawerItems: {
    paddingTop: 10,
    marginLeft: 40,
    fontSize: 17,
    fontWeight: headerFontWeight,
    color: headerTitleColor,
  },
};

export default styles;
