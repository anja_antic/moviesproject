// @flow
import React, { Component } from 'react';
import { FlatList, Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import type { Dispatch as ReduxDispatch } from 'redux';

import styles from './styles';
import { fetchGenres, getGenre } from '../actions/GenresActions';
import { keyExtractor } from '../utils/helpers';

type Props = {
  genres: Array<Object>,
  navigation: Object,
  getGenre: (itemId: number, itemName: string) => Object, // proveriti ispravnost
  fetchGenres: () => void,
};

class DrawerMenu extends Component<Props> {

  componentDidMount() {
    this.props.fetchGenres();
  }

  onGenrePress(itemId, itemName) {
    const { getGenre, navigation } = this.props;
    getGenre(itemId, itemName);
    navigation.navigate('SortedByGenreContainer');
    navigation.closeDrawer();
  }

  render() {
    const { genres } = this.props;
    return (
      <View style={styles.mainView}>
        <Text style={styles.title}>
          GENRE
        </Text>
        <View style={styles.lineSeparator} />
        <FlatList
          data={genres}
          keyExtractor={(item, index) => keyExtractor(item, index)}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={() => this.onGenrePress(item.id, item.name)}>
              <Text style={styles.drawerItems}>{item.name}</Text>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  genres: state.genres.genres,
});
const mapDispatchToProps = (dispatch: ReduxDispatch): Function => (
  bindActionCreators({
    fetchGenres,
    getGenre,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(DrawerMenu);
