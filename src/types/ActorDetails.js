// @flow
export type ActorDetails = {
  name: string,
  profilePath: string,
  birthday: string,
  deathday: string,
  place_of_birth: string,
  biography: string,
};
