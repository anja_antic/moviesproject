import { Dimensions } from 'react-native';
import { Fonts } from '../../utils/Fonts';
import { headerBackgroundColor, iconsColor } from '../../constants/styleConstants';

const styles = {
  mainView: {
    height: Dimensions.get('window').height / 3.7,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: iconsColor,
  },
  imageView: {
    width: Dimensions.get('window').width / 3,
  },
  image: {
    height: '100%',
  },
  textView: {
    flexDirection: 'column',
    width: Dimensions.get('window').width / 1.5,
    padding: 10,
    justifyContent: 'center',
    backgroundColor: headerBackgroundColor,
    opacity: 0.8,
  },
  textTitle: {
    fontFamily: Fonts.MontserratRegular,
    color: 'white',
    fontSize: 18,
    marginBottom: 10,
  },
  textOverview: {
    fontFamily: Fonts.MontserratLight,
    color: 'white',
    fontSize: 12,
  },
};

export default styles;
