// @flow
import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import Swipeout from 'react-native-swipeout';

import { deleteRed } from '../../constants/styleConstants';
import styles from './styles';
import { getPosterUrl } from '../../utils/helpers';
import type { Card } from '../../types/Card';

type Props = {
  navigation: Object,
  card: Card,
  removeFromFavourite: (movieItem: Card) => Object,
}

const FavouriteMovieCard = (props: Props) => {
  const { navigation, card, removeFromFavourite } = props;
  const swipeBtns = [{
    text: 'Delete',
    backgroundColor: deleteRed,
    onPress: () => removeFromFavourite(card),
  }];
  return (
    <Swipeout
      right={swipeBtns}
    >
      <TouchableOpacity onPress={() => navigation.navigate({
        routeName: 'MovieDetailsContainer',
        params: {
          card,
        },
      })}
      >
        <View style={styles.mainView}>
          <View style={styles.imageView}>
            <Image
              style={styles.image}
              resizeMode="cover"
              source={{ uri: getPosterUrl(card.poster_path) }}
            />
          </View>
          <View style={styles.textView}>
            <Text
              numberOfLines={2}
              style={styles.textTitle}
            >
              {card.title}
            </Text>
            <Text
              numberOfLines={3}
              style={styles.textOverview}
            >
              {card.overview}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </Swipeout>
  );
};

export default FavouriteMovieCard;
