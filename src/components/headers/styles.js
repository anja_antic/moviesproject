import {
  headerBackgroundColor,
  headerFontSize,
  headerFontWeight,
  headerHeight,
  headerTitleColor,
  iconsColor,
} from '../../constants/styleConstants';

const styles = {
  headerStyle: {
    backgroundColor: headerBackgroundColor,
    height: headerHeight,
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerTitle: {
    color: headerTitleColor,
    fontSize: headerFontSize,
    fontWeight: headerFontWeight,
    paddingTop: 30,
    marginLeft: 20,
  },
  hamburgerIconTouch: {
    marginLeft: 10,
    paddingTop: 20,
  },
  backIconTouch: {
    marginLeft: 20,
    paddingTop: 30,
  },
  backIcon: {
    width: 20,
    height: 20,
    color: iconsColor,
  },
  datePickView: {
    position: 'absolute',
    top: 23,
    right: 3,
  },
};

export default styles;
