// @flow
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import styles from './styles';

type Props = {
  navigation: Object,
  title: string,
};

const HeaderBackButton = (props: Props) => {
  const { title } = props;
  return (
    <View style={styles.headerStyle}>
      <TouchableOpacity
        onPress={() => props.navigation.navigate({ routeName: 'POPULAR' })}
        style={styles.backIconTouch}
      >
        <FontAwesome5 name="chevron-left" size={20} style={styles.backIcon} />
      </TouchableOpacity>
      <Text style={styles.headerTitle}>{title}</Text>
    </View>
  );
};

export default HeaderBackButton;
