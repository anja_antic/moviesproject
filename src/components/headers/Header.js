// @flow
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import styles from './styles';
import { iconsColor } from '../../constants/styleConstants';
import DatePick from '../datePick/DatePick';

type Props = {
  title: string,
  navigation: Object,
}

const Header = (props: Props) => {
  const { title, navigation } = props;
  return (
    <View>
      <View style={styles.headerStyle}>
        <TouchableOpacity
          onPress={() => navigation.openDrawer()}
          style={styles.hamburgerIconTouch}
        >
          <FontAwesome5 name="align-left" size={30} color={iconsColor} />
        </TouchableOpacity>
        <Text style={styles.headerTitle}>{title}</Text>
      </View>
      <View style={styles.datePickView}>
        <DatePick navigation={navigation} />
      </View>
    </View>
  );
};

export default Header;
