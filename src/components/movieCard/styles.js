import { Dimensions } from 'react-native';
import { Fonts } from '../../utils/Fonts';
import { headerTitleColor } from '../../constants/styleConstants';

const styles = {
  viewMovieCard: {
    width: Dimensions.get('window').width / 2,
    height: Dimensions.get('window').height / 2.5,
  },
  movieTape: {
    width: '100%',
    flex: 5,
    height: Dimensions.get('window').height / 18,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 10,
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#363D43',
    opacity: 0.8,
  },
  textTitle: {
    fontFamily: Fonts.MontserratRegular,
    color: 'white',
    fontSize: 10,
    flex: 4,
    marginRight: 15,
  },
  averageVote: {
    fontFamily: Fonts.MontserratRegular,
    color: 'white',
    fontSize: 8,
    flex: 1,
    marginRight: 1,
  },
  starIcon: {
    marginRight: 10,
    color: headerTitleColor,
  },
  poster: {
    width: '100%',
    height: '100%',
  },
};

export default styles;
