// @flow
import React from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import AddButton from '../addButton/AddButton';
import { getPosterUrl } from '../../utils/helpers';
import styles from './styles';
import type { Card } from '../../types/Card';

type Props = {
  navigation: Object,
  card: Card,
  addToFavourite: (movieItem: Card) => Object,
}


const MovieCard = (props: Props) => {
  const { navigation, card, addToFavourite } = props;
  return (
      <TouchableOpacity onPress={() => navigation.navigate({
        routeName: 'MovieDetailsContainer',
        params: {
          card,
        },
      })}
      >
        <View style={styles.viewMovieCard}>
          <Image
            resizeMode="cover"
            style={styles.poster}
            source={{ uri: getPosterUrl(card.poster_path) }}
          />
          <View style={styles.movieTape}>
            <Text
              numberOfLines={2}
              style={styles.textTitle}
            >
              {card.title}
            </Text>
            <Text
              style={styles.averageVote}
            >
              {`${card.vote_average}/10`}
            </Text>
            <FontAwesome5 name="star" size={8} style={styles.starIcon} solid />
          </View>
        </View>
        <View style={{ position: 'absolute', right: 15, top: 15 }}>
          <AddButton
            addToFavourite={() => addToFavourite(card)}
          />
        </View>
      </TouchableOpacity>
    );
};

export default MovieCard;
