// @flow
import React, { Component } from 'react';
import { FlatList, View } from 'react-native';

import { keyExtractor } from '../../utils/helpers';
import MovieCard from '../movieCard/MovieCard';
import type { Card } from '../../types/Card';
import SearchBox from '../searchBox/SearchBox';

type Props = {
  getMovies: () => Promise<void>,
  movies: Array<Card>,
  navigation: Object,
  addToFavourite: (movieItem: Card) => Object,
  removeFromFavourite: (movieItem: Card) => Object,
}

type State = {
  refreshing: boolean,
  chosenDate: string,
  searching: boolean,
}

class RefreshableFlatList extends Component<Props, State> {
  state = {
    page: 1,
    refreshing: false,
    searching: false,
  };

  componentDidMount() {
    this.onRefresh();
    console.log('comDidMountPAge', this.state.page);
  }

  onRefresh() {
    this.setState({ refreshing: true });
    this.props.getMovies(this.state.page)
      .then(this.setState({ refreshing: false }))
      .catch(error => console.log(error));
  }

  pagination() {
    this.props.getMovies(this.state.page + 1, true)
      .then(this.setState({ page: this.state.page + 1 }))
      .catch(error => console.log(error));
  }

  isSearching(bool: boolean) {
    this.setState({ searching: bool });
  }

  renderList() {
    if (this.state.searching === false) {
      return (
        <FlatList
          numColumns={2}
          onRefresh={() => this.onRefresh()}
          refreshing={this.state.refreshing}
          onEndReached={() => this.pagination()}
          onEndReachedThreshold={0.1}
          data={this.props.movies}
          keyExtractor={(item, index) => keyExtractor(item, index)}
          renderItem={({ item }) => (
            <MovieCard
              addToFavourite={() => this.props.addToFavourite(item)}
              removeFromFavourite={() => this.props.removeFromFavourite(item)}
              navigation={this.props.navigation}
              card={item}
            />
          )}
        />
      );
    }
    return null;
  }

  render() {
    console.log('searching', this.state.searching);
    console.log('allPagesMovies', this.props.movies);
    return (
      <View>
        <SearchBox
          isSearching={bool => this.isSearching(bool)}
          navigation={this.props.navigation}
          movies={this.props.movies}
        />
        {this.renderList()}
      </View>
    );
  }
}

export default RefreshableFlatList;
