// @flow
import React, { Component } from 'react';
import { DatePicker } from 'native-base'; // native-base je dodatno instaliran
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Text } from 'react-native';
import { bindActionCreators } from 'redux';
import type { Dispatch as ReduxDispatch } from 'redux';
import { connect } from 'react-redux';

import getMoviesByReleaseDate from '../../actions/ReleaseDateActions';
import styles from './styles';
import { formatTime } from '../../utils/helpers';

type Props = {
  getMoviesByReleaseDate: (date: string) => Object,
  navigation: Object,
}

type State = {
  chosenDate: string,
}

class DatePick extends Component<Props, State> {
  state = {
    chosenDate: '',
  };

  onDateChange(date: string) {
    this.setState({ chosenDate: formatTime(date) });
    this.props.getMoviesByReleaseDate(this.state.chosenDate);
    this.props.navigation.navigate({ routeName: 'SortedByReleaseDate' });
  }

  render() {
    return (
      <DatePicker
        defaultDate={this.state.chosenDate}
        minimumDate={new Date(1970, 1, 1)}
        maximumDate={new Date(2050, 12, 31)}
        placeHolderText={<Text><FontAwesome5 name="calendar-alt" size={25} /></Text>}
        textStyle={{ color: 'grey' }}
        placeHolderTextStyle={styles.placeHolderTextStyle}
        onDateChange={date => this.onDateChange(date)} // 'date' se odnosi na default date
      />
    );
  }
}

const mapDispatchToProps = (dispatch: ReduxDispatch): Function => (
  bindActionCreators({ getMoviesByReleaseDate }, dispatch)
);

export default connect(null, mapDispatchToProps)(DatePick);
