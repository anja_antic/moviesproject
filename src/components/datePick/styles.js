import { iconsColor } from '../../constants/styleConstants';

const styles = {
  placeHolderTextStyle: {
    color: iconsColor,
  },
};

export default styles;
