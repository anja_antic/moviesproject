// @flow
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import { addRemoveButtons } from '../../constants/styleConstants';
import type { Card } from '../../types/Card';

type Props = {
  removeFromFavourite: (movieItem: Card) => Object,
}

const RemoveButton = (props: Props) => {
  return (
    <TouchableOpacity onPress={() => props.removeFromFavourite()}>
      <View style={addRemoveButtons.button}>
        <Text style={addRemoveButtons.buttonText}>
          Remove from favourite
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default RemoveButton;
