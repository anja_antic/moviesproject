// @flow
import React from 'react';
import { View, StatusBar } from 'react-native';

const Statusbar = () => {
  return (
    <View>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="light-content"
      />
    </View>
  );
};

export default Statusbar;
