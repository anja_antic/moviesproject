import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import { Card } from '../../types/Card';
import { addRemoveButtons } from '../../constants/styleConstants';

type Props = {
  addToFavourite: (item: Card) => Object,
};

const AddButton = (props: Props) => {
  return (
    <TouchableOpacity onPress={() => props.addToFavourite()}>
      <View style={addRemoveButtons.button}>
        <Text style={addRemoveButtons.buttonText}>
          Add to favourite
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default AddButton;
