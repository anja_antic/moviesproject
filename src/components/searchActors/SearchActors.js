// @flow
import React, { Component } from 'react';
import { View, TextInput, FlatList } from 'react-native';

import { iconsColor, searchedMoviesPaddingBottom, searchBoxStyle } from '../../constants/styleConstants';
import { keyExtractor } from '../../utils/helpers';
import type { Actor } from '../../types/Actor';
import ActorCard from '../actorCard/ActorCard';

type Props = {
  isSearching: (bool: boolean) => void,
  actors: Array<Actor>,
  navigation: Object,
}

type State = {
  text: string,
  searchedActors: Array<Actor>
}

class SearchActors extends Component<Props, State> {
  state = {
    text: '',
    searchedActors: [],
  };

  search(text: string): void {
    const { isSearching, actors } = this.props;
    this.setState({ text });
    if (text.length < 3) {
      isSearching(false);
      this.setState({ searchedActors: [] });
      return;
    }
    this.setState({ text });
    const searchedActors = actors.filter(item => item.name.toLowerCase()
      .includes(text.toLowerCase()));
    this.setState({ searchedActors });
    isSearching(true);
    console.log('searchedActors', searchedActors);
  }

  listSearchedActors() {
    const { navigation } = this.props;
    const { searchedActors } = this.state;
    if (searchedActors.length > 0) {
      return (
        <View style={{ paddingBottom: searchedMoviesPaddingBottom }}>
          <FlatList
            numColumns={2}
            data={searchedActors}
            keyExtractor={(item, index) => keyExtractor(item, index)}
            renderItem={({ item }) => (
              <ActorCard
                navigation={navigation}
                actor={item}
              />
            )}
          />
        </View>
      );
    }
    return null;
  }

  render() {
    return (
      <View>
        <View style={searchBoxStyle.searchView}>
          <TextInput
            underlineColorAndroid="transparent"
            placeholderTextColor={iconsColor}
            style={searchBoxStyle.searchBox}
            placeholder="Search"
            onChangeText={text => this.search(text)}
            value={this.state.text}
          />
        </View>
        {this.listSearchedActors()}
      </View>
    );
  }
}

export default SearchActors;
