// @flow
import React, { Component } from 'react';
import { FlatList, TextInput, View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import type { Dispatch as ReduxDispatch } from 'redux';
import { connect } from 'react-redux';

import Statusbar from '../statusBar/Statusbar';
import { searchedMoviesPaddingBottom, iconsColor, searchBoxStyle } from '../../constants/styleConstants';
import { keyExtractor } from '../../utils/helpers';
import MovieCard from '../movieCard/MovieCard';
import { addToFavourite, removeFromFavourite } from '../../actions/FavouriteMovieActions';
import type { Card } from '../../types/Card';

type Props = {
  movies: Array<Card>,
  navigation: Object,
  addToFavourite: (item: Card) => Object,
  removeFromFavourite: (item: Card) => Object,
  isSearching: (bool: boolean) => boolean;
};

type State = {
  text: string,
  searchedMovies: Array<Card>,
}

class SearchBox extends Component<Props, State> {
  state = {
    text: '',
    searchedMovies: [],
  };

  search(text: string): void {
    const { isSearching, movies } = this.props;
    this.setState({ text });
    if (text.length < 3) {
      isSearching(false);
      this.setState({ searchedMovies: [] });
      return;
    }
    this.setState({ text });
    const searchedMovies = movies.filter(ele => ele.title.toLowerCase()
      .includes(text.toLowerCase()));
    this.setState({ searchedMovies });
    isSearching(true);
    console.log(searchedMovies);
  }

  renderList() {
    const { searchedMovies } = this.state;
    const { navigation, addToFavourite, removeFromFavourite } = this.props;
    if (searchedMovies.length > 0) {
      return (
        <View style={{ paddingBottom: searchedMoviesPaddingBottom }}>
          <FlatList
            numColumns={2}
            data={searchedMovies}
            keyExtractor={(item, index) => keyExtractor(item, index)}
            renderItem={({ item }) => (
              <MovieCard
                addToFavourite={() => addToFavourite(item)}
                removeFromFavourite={() => removeFromFavourite(item)}
                navigation={navigation}
                card={item}
              />
            )}
          />
        </View>
      );
    }
    return null;
  }

  render() {
    return (
      <View>
        <View style={searchBoxStyle.searchView}>
          <TextInput
            underlineColorAndroid="transparent"
            placeholderTextColor={iconsColor}
            style={searchBoxStyle.searchBox}
            placeholder="Search"
            onChangeText={text => this.search(text)}
            value={this.state.text}
          />
        </View>
        <Statusbar />
        {this.renderList()}
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch: ReduxDispatch): Function => (
  bindActionCreators({
    addToFavourite,
    removeFromFavourite,
  }, dispatch)
);

export default connect(null, mapDispatchToProps)(SearchBox);
