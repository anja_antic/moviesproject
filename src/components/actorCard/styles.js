import { Dimensions } from 'react-native';

import { headerBackgroundColor } from '../../constants/styleConstants';
import { Fonts } from '../../utils/Fonts';

const styles = {
  cardView: {
    width: Dimensions.get('window').width / 2,
    height: Dimensions.get('window').width / 2,
  },
  imageView: {
    width: '100%',
    height: '100%',
  },
  textView: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: headerBackgroundColor,
    opacity: 0.8,
    padding: 3,
  },
  textStyle: {
    fontFamily: Fonts.MontserratRegular,
    color: 'white',
    fontSize: 11,
  },
};

export default styles;
