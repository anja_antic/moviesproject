// @flow
import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Transition } from 'react-navigation-fluid-transitions';

import { getProfileUrl } from '../../utils/helpers';
import type { Actor } from '../../types/Actor';
import styles from './styles';

type Props = {
  actor: Actor,
  navigation: Object,
}

const ActorCard = (props: Props) => {
  const { actor } = props;
  return (
    <TouchableOpacity onPress={() => props.navigation.navigate({
      routeName: 'ActorDetailsContainer',
      params: {
        actorId: actor.id,
        profilePath: actor.profile_path,
      },
    })}
    >
      <View style={styles.cardView}>
        <View style={styles.imageView}>
          <Transition shared={`image${actor.id}`}>
            <Image
              resizeMode="cover"
              source={{ uri: getProfileUrl(actor.profile_path) }}
              style={{ width: '100%', height: '100%' }}
            />
          </Transition>
        </View>
        <View style={styles.textView}>
          <Text style={styles.textStyle}>{actor.name}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default ActorCard;
