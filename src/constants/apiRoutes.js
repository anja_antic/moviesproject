const API_BASE = 'https://api.themoviedb.org';
const IMAGE_BASE = 'http://image.tmdb.org/t/p';

const POPULAR_MOVIES = `${API_BASE}/3/movie/popular?api_key=9db7219819199b8a0ac037208bdf0233&language=en-US&page=<page>`;
const UPCOMING_MOVIES = `${API_BASE}/3/movie/upcoming?api_key=9db7219819199b8a0ac037208bdf0233&language=en-US&page=<page>`;
const TOP_RATED = `${API_BASE}/3/movie/top_rated?api_key=9db7219819199b8a0ac037208bdf0233&language=en-US&page=<page>`;
const POSTER_PATH = `${IMAGE_BASE}/w500`;
const PROFILE_PATH = `${IMAGE_BASE}/original`;
const GENRES = `${API_BASE}/3/genre/movie/list?api_key=9db7219819199b8a0ac037208bdf0233&language=en-US`;
const VIDEO = `${API_BASE}/3/movie/<id>/videos?api_key=9db7219819199b8a0ac037208bdf0233&language=en-US`;
const ACTORS = `${API_BASE}/3/person/popular?api_key=9db7219819199b8a0ac037208bdf0233&language=en-US&page=<page>`;
const ACTOR_DETAILS = `${API_BASE}/3/person/<id>?api_key=9db7219819199b8a0ac037208bdf0233&language=en-US`;

export {
  POPULAR_MOVIES,
  UPCOMING_MOVIES,
  TOP_RATED,
  POSTER_PATH,
  PROFILE_PATH,
  GENRES,
  VIDEO,
  ACTORS,
  ACTOR_DETAILS,
};
