// GET MOVIES
const FETCH_POPULAR_MOVIES = 'fetch_popular_movies';
const FETCH_UPCOMING_MOVIES = 'fetch_upcoming_movies';
const FETCH_TOP_RATED_MOVIES = 'fetch_top_rated_movies';
const GET_MORE_POPULAR = 'get_more_popular';
const GET_MORE_UPCOMING = 'get_more_upcoming';
const GET_MORE_TOP_RATED = 'get_more_top_rated';

// FAVOURITE
const ADD_TO_FAVOURITE = 'add_to_favourite';
const REMOVE_FROM_FAVOURITE = 'remove_from_favourite';

// GENRES
const FETCH_GENRES = 'fetch_genres';
const GET_GENRE = 'get_genre';

// const IS_SEARCHING = 'is_searching';

// RELEASE DATE
const FIND_MOVIES_BY_RELEASE_DATE = 'find_movies_by_release_date';

// VIDEO
const GET_VIDEO = 'get_video';
const SET_VIDEO_URL_EMPTY = 'set_video_url_empty';

// ACTORS
const GET_ACTORS = 'get_actors';
const GET_ACTOR_DETAILS = 'get_actor_details';
const CLEAR_ACTOR_DETAILS = 'clear_actor_details';
const GET_MORE_ACTORS = 'get_more_actors';

export {
  FETCH_POPULAR_MOVIES,
  FETCH_UPCOMING_MOVIES,
  FETCH_TOP_RATED_MOVIES,
  ADD_TO_FAVOURITE,
  REMOVE_FROM_FAVOURITE,
  GET_MORE_POPULAR,
  GET_MORE_UPCOMING,
  GET_MORE_TOP_RATED,
  FETCH_GENRES,
  GET_GENRE,
  FIND_MOVIES_BY_RELEASE_DATE,
  GET_VIDEO,
  SET_VIDEO_URL_EMPTY,
  GET_ACTORS,
  GET_ACTOR_DETAILS,
  CLEAR_ACTOR_DETAILS,
  GET_MORE_ACTORS,
};
