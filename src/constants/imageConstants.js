const CloseOnWhite = require('../../assets/icons/closeonwhite.png');
const FavouriteActiveWhite = require('../../assets/icons/favouriteactivewhite.png');
const FavouriteFill = require('../../assets/icons/favouritefill.png');
const Previous = require('../../assets/icons/previous.png');

export { CloseOnWhite, FavouriteActiveWhite, FavouriteFill, Previous };
