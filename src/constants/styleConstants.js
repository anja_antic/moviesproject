import { Dimensions, Platform } from 'react-native';
import { Fonts } from '../utils/Fonts';

const headerBackgroundColor = '#363D43';
const headerTitleColor = '#EFBD1F';
const iconsColor = '#62666A';
const deleteRed = '#cc0000';
const headerHeight = 70;
const headerFontSize = 17;
const headerFontWeight = Platform.OS === 'android' ? '500' : '600';
const headerTitleMarginTop = 25;
const listsPaddingBottom = 255;
const sortedByGenrePaddingBottom = 129;
const searchedMoviesPaddingBottom = 120;
const favouriteMoviesPaddingBottom = 70;

const backButtonInDetails = {
  backButtonTouch: {
    position: 'absolute',
    right: 20,
    top: 30,
  },
  backButtonIcon: {
    width: 40,
    height: 40,
  },
};

const searchBoxStyle = {
  searchView: {
    backgroundColor: iconsColor,
  },
  searchBox: {
    margin: 5,
    paddingLeft: 25,
    width: Dimensions.get('window').width / 1.1,
    alignSelf: 'center',
    backgroundColor: headerBackgroundColor,
    borderRadius: 25,
    color: 'white',
    fontSize: 14,
  },
};

const addRemoveButtons = {
  button: {
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 7,
    backgroundColor: headerBackgroundColor,
    opacity: 0.8,
    padding: 7,
  },
  buttonText: {
    fontFamily: Fonts.MontserratRegular,
    color: 'white',
    fontSize: 9,
  },
};

export {
  headerBackgroundColor,
  headerTitleColor,
  deleteRed,
  iconsColor,
  headerHeight,
  headerFontSize,
  headerFontWeight,
  headerTitleMarginTop,
  listsPaddingBottom,
  sortedByGenrePaddingBottom,
  searchedMoviesPaddingBottom,
  backButtonInDetails,
  searchBoxStyle,
  favouriteMoviesPaddingBottom,
  addRemoveButtons,
};
