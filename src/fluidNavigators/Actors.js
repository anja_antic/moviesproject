import { createFluidNavigator } from 'react-navigation-fluid-transitions';

import ActorsContainer from '../containers/actorsContainer/ActorsContainer';
import ActorDetailsContainer from '../containers/actorDetailsContainer/ActorDetailsContainer';

const Actors = createFluidNavigator({
  ActorsContainer: {
    screen: ActorsContainer,
  },
  ActorDetailsContainer: {
    screen: ActorDetailsContainer,
  },
});

Actors.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
  };
};

export default Actors;
