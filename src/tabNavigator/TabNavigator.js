import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import { headerBackgroundColor, headerTitleColor, iconsColor } from '../constants/styleConstants';
import Popular from '../stackNavigators/Popular';
import TopRated from '../stackNavigators/TopRated';
import Upcoming from '../stackNavigators/Upcoming';
import Favourites from '../stackNavigators/Favourites';
import Actors from '../fluidNavigators/Actors';

const Tab = createBottomTabNavigator({
  POPULAR: {
    screen: Popular,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <FontAwesome5 name="grin-stars" size={23} solid color={tintColor} />
      ),
    },
  },
  UPCOMING: {
    screen: Upcoming,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <FontAwesome5 name="spinner" size={23} color={tintColor} />
      ),
    },
  },
  'TOP RATED': {
    screen: TopRated,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <FontAwesome5 name="star" size={23} solid color={tintColor} />
      ),
    },
  },
  FAVOURITES: {
    screen: Favourites,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <FontAwesome5 name="heart" size={23} solid color={tintColor} />
      ),
    },
  },
  ACTORS: {
    screen: Actors,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <FontAwesome5 name="users" size={23} color={tintColor} />
      ),
    },
  },
},
{
  tabBarOptions: {
    showIcon: true,
    showLabel: true,
    labelStyle: {
      fontSize: 8,
    },
    style: {
      backgroundColor: headerBackgroundColor,
      height: 40,
    },
    activeTintColor: headerTitleColor,
    inactiveTintColor: iconsColor,
  },
});

export default Tab;
