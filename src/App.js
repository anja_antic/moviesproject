import './ReactotronConfig';

import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { persistStore, persistReducer } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';

import reducers from './reducers';
import DrawerNavigator from './drawerNavigator/DrawerNavigator';

class App extends Component {
  render() {
    const persistConfig = {
      key: 'root',
      storage: AsyncStorage,
      whitelist: ['favouriteMovies'],
    };
    const persistedReducer = persistReducer(persistConfig, reducers);
    const store = createStore(persistedReducer, {}, applyMiddleware(ReduxThunk));
    const persistor = persistStore(store);
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <DrawerNavigator />
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
// PersistGate odlaze renderovanje aplikacije sve dok persisted state nije dostupan i memorisan u redux
// loading={null} - dok se ucitava nemam neki spiner itd... U viticastim zagradama moze da se stavi i neka komponenta
