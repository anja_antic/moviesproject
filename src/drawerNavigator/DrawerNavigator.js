import { createDrawerNavigator } from 'react-navigation';

import Tab from '../tabNavigator/TabNavigator';
import { headerBackgroundColor } from '../constants/styleConstants';
import DrawerMenu from '../drawerMenu/DrawerMenu';
import Genre from '../stackNavigators/Genre';
import ReleaseDate from '../stackNavigators/ReleaseDate';

const DrawerNavigator = createDrawerNavigator({
  Home: {
    screen: Tab,
  },
  SortedByGenreContainer: {
    screen: Genre,
  },
  SortedByReleaseDate: {
    screen: ReleaseDate,
  },
}, {
  contentComponent: DrawerMenu,
  drawerBackgroundColor: headerBackgroundColor,
  statusBarBackgroundColor: 'transparent',
});

export default DrawerNavigator;
