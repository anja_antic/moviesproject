import { createStackNavigator } from 'react-navigation';

import SortedByGenreContainer from '../containers/sortedByGenreContainer/SortedByGenreContainer';
import MovieDetailsContainer from '../containers/movieDetailsContainer/MovieDetailsContainer';

const Genre = createStackNavigator({
  SortedByGenreContainer: {
    screen: SortedByGenreContainer,
    navigationOptions: {
      header: null,
    },
  },
  MovieDetailsContainer: {
    screen: MovieDetailsContainer,
    navigationOptions: {
      header: null,
    },
  },
});

export default Genre;
