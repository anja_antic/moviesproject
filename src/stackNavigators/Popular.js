import { createStackNavigator } from 'react-navigation';
import ListOfPopularContainer from '../containers/popularContainer/ListOfPopularContainer';
import MovieDetailsContainer from '../containers/movieDetailsContainer/MovieDetailsContainer';

const Popular = createStackNavigator({
  ListOfPopularContainer: {
    screen: ListOfPopularContainer,
    navigationOptions: {
      header: null,
    },
  },
  MovieDetailsContainer: {
    screen: MovieDetailsContainer,
    navigationOptions: {
      header: null,
    },
  },
});

Popular.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
  };
};

export default Popular;
