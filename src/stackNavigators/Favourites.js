import { createStackNavigator } from 'react-navigation';

import FavouriteMoviesContainer from '../containers/favouriteMoviesContainer/FavouriteMoviesContainer';
import MovieDetailsContainer from '../containers/movieDetailsContainer/MovieDetailsContainer';

const Favourites = createStackNavigator({
  ListOfPopularContainer: {
    screen: FavouriteMoviesContainer,
    navigationOptions: {
     header: null,
    },
  },
  MovieDetailsContainer: {
    screen: MovieDetailsContainer,
    navigationOptions: {
      header: null,
    },
  },
});

Favourites.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
  };
};

export default Favourites;
