import { createStackNavigator } from 'react-navigation';

import ListOfUpcomingContainer from '../containers/upcomingContainer/ListOfUpcomingContainer';
import MovieDetailsContainer from '../containers/movieDetailsContainer/MovieDetailsContainer';

const Upcoming = createStackNavigator({
  ListOfUpcomingContainer: {
    screen: ListOfUpcomingContainer,
    navigationOptions: {
      header: null,
    },
  },
  MovieDetailsContainer: {
    screen: MovieDetailsContainer,
    navigationOptions: {
      header: null,
    },
  },
});

Upcoming.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
  };
};

export default Upcoming;
