import { createStackNavigator } from 'react-navigation';

import ListOfTopRatedContainer from '../containers/topRatedContainer/ListOfTopRatedContainer';
import MovieDetailsContainer from '../containers/movieDetailsContainer/MovieDetailsContainer';

const TopRated = createStackNavigator({
  ListOfTopRatedContainer: {
    screen: ListOfTopRatedContainer,
    navigationOptions: {
      header: null,
    },
  },
  MovieDetailsContainer: {
    screen: MovieDetailsContainer,
    navigationOptions: {
      header: null,
    },
  },
});

TopRated.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
  };
};

export default TopRated;
