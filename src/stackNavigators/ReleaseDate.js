import { createStackNavigator } from 'react-navigation';

import SortedByReleaseDate from '../containers/sortedByReleaseDate/SortedByReleaseDate';
import MovieDetailsContainer from '../containers/movieDetailsContainer/MovieDetailsContainer';

const ReleaseDate = createStackNavigator({
  SortedByReleaseDate: {
    screen: SortedByReleaseDate,
    navigationOptions: {
     header: null,
    },
  },
  MovieDetailsContainer: {
    screen: MovieDetailsContainer,
    navigationOptions: {
      header: null,
    },
  },
});

export default ReleaseDate;
