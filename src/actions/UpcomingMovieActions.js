// @flow
import axios from 'axios';
import { Dispatch as ReduxDispatch } from 'redux';

import { FETCH_UPCOMING_MOVIES, GET_MORE_UPCOMING } from '../constants/reduxActionsConstants';
import { UPCOMING_MOVIES } from '../constants/apiRoutes';

const fetchUpcomingMovies = (page: number, isRefresh: boolean = false) => (dispatch: ReduxDispatch): Promise<void> => {
  return new Promise((resolve, reject) => {
    const url = UPCOMING_MOVIES.replace('<page>', page);
    console.log(url);
    axios.get(url)
      .then((response) => {
        resolve();
        console.log(response);
        let type = FETCH_UPCOMING_MOVIES;
        if (isRefresh) {
          type = GET_MORE_UPCOMING;
        }
        dispatch({
          type,
          payload: response.data,
        });
      })
      .catch((error) => {
        reject();
        console.log(error);
      });
  });
};

export default fetchUpcomingMovies;
