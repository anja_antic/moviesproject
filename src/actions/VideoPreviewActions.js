// @flow
import axios from 'axios';
import type { Dispatch as ReduxDispatch } from 'redux';

import { VIDEO } from '../constants/apiRoutes';
import { GET_VIDEO, SET_VIDEO_URL_EMPTY } from '../constants/reduxActionsConstants';

export const getVideo = (id: number) => (dispatch: ReduxDispatch): void => {
  const url = VIDEO.replace('<id>', id);
  axios.get(url)
    .then((response) => {
      console.log(response);
      dispatch({
        type: GET_VIDEO,
        payload: response.data,
      });
    })
    .catch((error) => {
      console.log('error', error);
    });
};

export const setVideoUrlEmpty = (): Object => {
  return {
    type: SET_VIDEO_URL_EMPTY,
  };
};
