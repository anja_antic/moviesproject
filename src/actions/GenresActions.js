// @flow
import axios from 'axios';
import type { Dispatch as ReduxDispatch } from 'redux';

import { GENRES } from '../constants/apiRoutes';
import { FETCH_GENRES, GET_GENRE } from '../constants/reduxActionsConstants';

export const fetchGenres = () => (dispatch: ReduxDispatch): void => {
  axios.get(GENRES)
    .then((response) => {
      console.log(response);
      dispatch({
        type: FETCH_GENRES,
        payload: response.data,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

export const getGenre = (itemId: number, itemName: string): Object => {
  return {
    type: GET_GENRE,
    id: itemId,
    name: itemName,
  };
};
