// @flow
import { ADD_TO_FAVOURITE, REMOVE_FROM_FAVOURITE, TOGGLE_FAV_BUTTON } from '../constants/reduxActionsConstants';
import type { Card } from '../types/Card';

export const addToFavourite = (movieItem: Card): Object => {
  return {
    type: ADD_TO_FAVOURITE,
    payload: movieItem,
  };
};

export const removeFromFavourite = (movieItem: Card): Object => {
  return {
    type: REMOVE_FROM_FAVOURITE,
    payload: movieItem,
  };
};

