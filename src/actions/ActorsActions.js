// @flow
import axios from 'axios';
import type { Dispatch as ReduxDispatch } from 'redux';

import {
  GET_ACTORS,
  GET_ACTOR_DETAILS,
  CLEAR_ACTOR_DETAILS,
  GET_MORE_ACTORS,
} from '../constants/reduxActionsConstants';
import { ACTORS, ACTOR_DETAILS } from '../constants/apiRoutes';

const getActors = (page: number, isRefresh: boolean = false) => (dispatch: ReduxDispatch) => {
  const url = ACTORS.replace('<page>', page);
  axios.get(url)
    .then((response) => {
      console.log('actors', response);
      let type = GET_ACTORS;
      if (isRefresh) {
        type = GET_MORE_ACTORS;
      }
      dispatch({
        type,
        payload: response.data,
      });
    })
    .catch(error => console.log(error));
};

const getActorDetails = (id: number) => (dispatch: ReduxDispatch) => {
  const url = ACTOR_DETAILS.replace('<id>', id);
  console.log(url);
  axios.get(url)
    .then((response) => {
      console.log('actorDetails', response);
      dispatch({
        type: GET_ACTOR_DETAILS,
        payload: response.data,
      });
    })
    .catch(error => console.log('actors error', error));
};

const clearActorDetails = () => {
  return {
    type: CLEAR_ACTOR_DETAILS,
  };
};

export { getActors, getActorDetails, clearActorDetails };
