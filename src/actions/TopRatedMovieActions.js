// @flow
import axios from 'axios';
import type { Dispatch as ReduxDispatch } from 'redux';

import { TOP_RATED } from '../constants/apiRoutes';
import {
  FETCH_TOP_RATED_MOVIES,
  GET_MORE_TOP_RATED,
} from '../constants/reduxActionsConstants';


const fetchTopRatedMovies = (page: number, isRefresh: boolean = false) => (dispatch: ReduxDispatch): Promise<void> => {
  return new Promise((resolve, reject) => {
    const url = TOP_RATED.replace('<page>', page);
    console.log(url);
    axios.get(url)
      .then((response) => {
        resolve();
        console.log(response);
        let type = FETCH_TOP_RATED_MOVIES;
        if (isRefresh) {
          type = GET_MORE_TOP_RATED;
        }
        dispatch({
          type,
          payload: response.data,
        });
      })
      .catch((error) => {
        reject();
        console.log(error);
      });
  });
};

export default fetchTopRatedMovies;
