// @flow
import { FIND_MOVIES_BY_RELEASE_DATE } from '../constants/reduxActionsConstants';

const getMoviesByReleaseDate = (date: string): Object => {
  return {
    type: FIND_MOVIES_BY_RELEASE_DATE,
    payload: date,
  };
};

export default getMoviesByReleaseDate;
