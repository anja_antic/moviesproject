// @flow
import axios from 'axios';
import type { Dispatch as ReduxDispatch } from 'redux';

import { FETCH_POPULAR_MOVIES, GET_MORE_POPULAR } from '../constants/reduxActionsConstants';
import { POPULAR_MOVIES } from '../constants/apiRoutes';


export const fetchPopularMovies = (page: number, isRefresh: boolean = false) => (dispatch: ReduxDispatch): Promise<void> => {
  return new Promise((resolve, reject) => {

    const url = POPULAR_MOVIES.replace('<page>', page);
    console.log(url);
    axios.get(url)
      .then((response) => {
        resolve();
        console.log(response);
        let type = FETCH_POPULAR_MOVIES;
        if (isRefresh) {
          type = GET_MORE_POPULAR;
        }
        dispatch({
          type,
          payload: response.data,
        });
      })
      .catch((error) => {
        reject();
        console.log(error);
      });
  });
};

