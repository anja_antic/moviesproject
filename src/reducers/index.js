import { combineReducers } from 'redux';
import PopularMovieReducer from './PopularMovieReducer';
import UpcomingMovieReducer from './UpcomingMovieReducer';
import TopRatedMovieReducer from './TopRatedMovieReducer';
import FavouriteMovieReducer from './FavouriteMovieReducer';
import GenresReducer from './GenresReducer';
import VideoPreviewReducer from './VideoPreviewReducer';
// import SearchingReducer from './SearchingReducer';
import ActorsReducer from './ActorsReducer';

export default combineReducers({
  popularMovies: PopularMovieReducer,
  upcoming: UpcomingMovieReducer,
  topRated: TopRatedMovieReducer,
  favouriteMovies: FavouriteMovieReducer,
  genres: GenresReducer,
  videoUrl: VideoPreviewReducer,
  // searching: SearchingReducer,
  actors: ActorsReducer,
});
