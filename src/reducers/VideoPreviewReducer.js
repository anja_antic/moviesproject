import { GET_VIDEO, SET_VIDEO_URL_EMPTY } from '../constants/reduxActionsConstants';

const INITIAL_STATE = {
  videoUrl: '',
};

export default (state = INITIAL_STATE, action) => {
  const newState = {
    videoUrl: state.videoUrl,
  };
  switch (action.type) {
    case GET_VIDEO:
      const trailerKeys = action.payload.results.filter(item => item.type === 'Trailer');
      const finalKey = trailerKeys[0].key;
      newState.videoUrl = finalKey;

      // TODO ukoliko nema videa sa tipom 'trailer', prikazati bilo koji da ne bi pokazivalo gresku u konzoli

      console.log('keys', trailerKeys);
      console.log('FINAL KEY', finalKey);
      console.log('video url', newState.videoUrl);
      return newState;
    case SET_VIDEO_URL_EMPTY:
      newState.videoUrl = '';
      return newState;
    default:
      return state;
  }
};
