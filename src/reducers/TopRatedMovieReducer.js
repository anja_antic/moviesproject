import { FETCH_TOP_RATED_MOVIES, GET_MORE_TOP_RATED } from '../constants/reduxActionsConstants';

const INITIAL_STATE = {
  topRatedMovies: [],
};

export default (state = INITIAL_STATE, action) => {
  const newState = {
    topRatedMovies: [...state.topRatedMovies],
  };

  switch (action.type) {
    case FETCH_TOP_RATED_MOVIES:
      newState.topRatedMovies = action.payload.results;
      console.log(newState);
      return newState;
    case GET_MORE_TOP_RATED:
      newState.topRatedMovies = [...newState.topRatedMovies, ...action.payload.results];
      console.log('nextPageTopRated', newState);
      return newState;
    default:
      return state;
  }
};
