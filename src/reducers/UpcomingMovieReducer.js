import { FETCH_UPCOMING_MOVIES, GET_MORE_UPCOMING } from '../constants/reduxActionsConstants';

const INITIAL_STATE = {
  upcomingMovies: [],
};

export default (state = INITIAL_STATE, action) => {
  const newState = {
    upcomingMovies: [...state.upcomingMovies],
  };

  switch (action.type) {
    case FETCH_UPCOMING_MOVIES:
      newState.upcomingMovies = action.payload.results;
      console.log(newState);
      return newState;
    case GET_MORE_UPCOMING:
      newState.upcomingMovies = [...newState.upcomingMovies, ...action.payload.results];
      console.log('nextPageUpcoming', newState);
      return newState;
    default:
      return state;
  }
};
