import {
  FETCH_POPULAR_MOVIES,
  GET_MORE_POPULAR,
  GET_GENRE,
  FIND_MOVIES_BY_RELEASE_DATE,
} from '../constants/reduxActionsConstants';

import { formatDate } from '../utils/helpers';

const INITIAL_STATE = {
  popularMovies: [],
  genre: [],
  genreName: '',
  moviesByDate: [],
  chosenDate: '',
};

export default (state = INITIAL_STATE, action) => {
  const newState = {
    popularMovies: [...state.popularMovies],
    genre: [...state.genre],
    genreName: state.genreName,
    moviesByDate: [...state.moviesByDate],
    chosenDate: state.chosenDate,
  };
  switch (action.type) {
    case FETCH_POPULAR_MOVIES:
      newState.popularMovies = action.payload.results;
      console.log(newState);
      return newState;
    case GET_MORE_POPULAR:
      newState.popularMovies = [...newState.popularMovies, ...action.payload.results];
      return newState;
    case GET_GENRE:
      newState.genreName = action.name.toUpperCase();
      newState.genre = newState.popularMovies.filter((item) => {
        for (let i = 0; i < item.genre_ids.length; i++) {
          if (item.genre_ids[i] === action.id) {
            return true;
          }
        }
        return false;
      });
      console.log('specific genre', newState.genre);
      return newState;
    case FIND_MOVIES_BY_RELEASE_DATE:
      newState.chosenDate = action.payload;
      newState.moviesByDate = newState.popularMovies.filter((item) => {
        if (formatDate(item.release_date) === action.payload) {
          return true;
        }
        return false;
      });
      console.log('releaseDateMovies', newState.moviesByDate);
      return newState;
    default:
      return state;
  }
};
