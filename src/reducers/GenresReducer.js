import { FETCH_GENRES } from '../constants/reduxActionsConstants';

const INITIAL_STATE = {
  genres: [],
};

export default (state = INITIAL_STATE, action) => {
  const newState = {
    genres: [...state.genres],
  };
  switch (action.type) {
    case FETCH_GENRES:
      newState.genres = action.payload.genres;
      console.log(newState.genres);
      return newState;
    default:
      return state;
  }
};
