import { REHYDRATE } from 'redux-persist/lib/constants';
import { ADD_TO_FAVOURITE, REMOVE_FROM_FAVOURITE } from '../constants/reduxActionsConstants';

const INITIAL_STATE = {
  favouriteMovies: [],
};

export default (state = INITIAL_STATE, action) => {
  const newState = {
    favouriteMovies: [...state.favouriteMovies],
  };
  switch (action.type) {
    case REHYDRATE:
      newState.favouriteMovies = action.payload !== undefined ? action.payload.favouriteMovies.favouriteMovies: [];
      return { ...newState };
    case ADD_TO_FAVOURITE:
      const isItemExist = newState.favouriteMovies.filter(item => item.id === action.payload.id).length > 0;
      if (!isItemExist) {
        newState.favouriteMovies.push(action.payload);
      }
      console.log('favourites', newState.favouriteMovies);
      return newState;
    case REMOVE_FROM_FAVOURITE:
      newState.favouriteMovies = newState.favouriteMovies.filter(item => item !== action.payload);
      console.log('favourites after remove', newState.favouriteMovies);
      return newState;
    default:
      return state;
  }
};
