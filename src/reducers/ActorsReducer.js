import {
  GET_ACTORS,
  GET_ACTOR_DETAILS,
  CLEAR_ACTOR_DETAILS,
  GET_MORE_ACTORS,
} from '../constants/reduxActionsConstants';

const INITIAL_STATE = {
  actors: [],
  activeActor: {},
};

export default (state = INITIAL_STATE, action) => {
  const newState = {
    actors: [...state.actors],
    activeActor: { ...state.activeActor },
  };
  switch (action.type) {
    case GET_ACTORS:
      newState.actors = action.payload.results;
      return newState;
    case GET_MORE_ACTORS:
      newState.actors = [...newState.actors, ...action.payload.results];
      return newState;
    case GET_ACTOR_DETAILS:
      newState.activeActor = action.payload;
      return newState;
    case CLEAR_ACTOR_DETAILS:
      newState.activeActor = {};
      console.log('activeActorAfterClear', newState.activeActor);
      return newState;
    default:
      return state;
  }
};
