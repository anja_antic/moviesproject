export const Fonts = {
  MontserratRegular: 'Montserrat-Regular',
  MontserratBlack: 'Montserrat-Black',
  MontserratLight: 'Montserrat-Light',
};
