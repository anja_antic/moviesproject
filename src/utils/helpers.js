// @flow
import { POSTER_PATH, PROFILE_PATH } from '../constants/apiRoutes';

// GET POSTER URL
const getPosterUrl = (imageUrl: string) => {
  return `${POSTER_PATH}${imageUrl}`;
};

// GET PROFILE URL
const getProfileUrl = (imageUrl: string) => {
  return `${PROFILE_PATH}${imageUrl}`;
};

// KEY EXTRACTOR
const keyExtractor = (item: Object, index: number) => `${item.id} + ${index}`;

// FORMAT DATE FROM API
const formatDate = (dateString: string) => {
  if (!dateString) {
    return '';
  }
  // zbog componentDidMount u movieDetails, ne dobijam datum odmah
  const day1 = dateString.slice(8);
  let day2 = day1;
  if (day1.charAt(0) === '0') {
    day2 = day1.slice(1);
  }

  const month1 = dateString.slice(5, 7);
  let month2 = month1;
  if (month1.charAt(0) === '0') {
    month2 = month1.slice(1);
  }

  const year = dateString.slice(0, 4);
  return `${day2}/${month2}/${year}`;
};

// FORMAT TIME FROM DATE PICKER
const formatTime = (time: string) => {
  return `${time.getDate()}/${time.getMonth() + 1}/${time.getFullYear()}`;
};

export { getPosterUrl, getProfileUrl, keyExtractor, formatDate, formatTime };
